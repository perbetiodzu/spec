## Allowlist tab
[Back to top of page](#options-page)

### Allowlist tab empty
![](/res/abp/desktop-settings/allowlisted-websites.jpg)

1. [Allowlist tab headline](#allowlist-tab-headline)
1. [Allowlist tab description](#allowlist-tab-description)
1. [Allowlist text input](#text-input)
1. [Add allowlisted domain button](#add-allowlisted-domain-button)
1. [Add allowlisted domain error message](#add-allowlisted-domain-error-message)
1. [Empty Allowlist placeholder](#empty-allowlist-placeholder)

#### Allowlist tab headline
`Allowlisted websites`

Navigation label will match headline.

#### Allowlist tab description
`You’ve turned off ad blocking on these websites and, therefore, will see ads on them. [Learn more][1]`

[1]: [Documentation link](/spec/abp/prefs.md#documentation-link) *allowlist*

#### Allowlisting a website
##### Behaviour
When a duplicate entry is made, move the original entry to the top of the list and discard the duplicate.

Convert URLs into domains when adding domain to the allowlist.

##### Text input 
Text input to enter domains to be added to the allowlist.

##### Label in text input 
`e.g. www.example.com`

#### Add allowlisted domain button
##### Overview
Button to submit [Allowlist text input](#text-input).

If clicked allowlist domain and show [Allowlisted domain added notification](#allowlisted-domain-added-notification).

Refer to [w3 guidelines for keyboard interaction](https://www.w3.org/TR/2016/WD-wai-aria-practices-1.1-20161214/#button)

> An allowlisted domain is an exception rule filter for a certain host.

##### Button label 
`Add website`

#### Add allowlisted domain error message
Shows an error message if [Add allowlisted domain text input](#text-input) doesn't validate.

#### Empty Allowlist placeholder
Placeholder text shown as long as there are no allowlisted domains.

`You don't have any websites in your allowlist.`

`Websites you trust and want to allow ads on will be shown here.`

### Allowlist tab populated
![](/res/abp/desktop-settings/allowlisted-websites-populated.png)

#### List of allowlisted domains
##### Overview
Displays all allowlisted domains. 

##### Behaviour
List items are sorted alphabetically when loaded.

Move newly added items (from the current session) to the top of the list.

When a duplicate domain entry is made, move the original entry to the top of the list.

<!-- This is to technical, find a better way to describe it -->
***Note***: *All allowlisted domains* does only refer to exception rules that match the following regexp and a member of the *SpecialSubscription* filter list:

```javascript
/^@@\|\|([^\/:]+)\^\$document$/
```

#### Remove allowlisted domain link

Clicking on the bin icon deletes the corresponding domain from the allowlist.

### Allowlisted domain added notification
![](/res/abp/desktop-settings/allowlisted-websites-notification.png)

#### Allowlisted domain added notification
##### Overview
The notification should appear as an overlay sliding in from the top of the page.

The notification will disappear if the X is clicked or automatically after 3 seconds.

##### Notification message
`Website has been added to the allowlist.`

#### Allowlisted Webpage URL from Bubble UI
- When specific webpage URL was allowlisted from Bubble UI, show the entire address without http(s) and www.
- If the URL is too long, truncate with `...` at the end
